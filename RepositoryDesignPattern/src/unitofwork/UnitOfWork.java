package unitofwork;
import domain.Entity;


public interface UnitOfWork {

	void saveChanges();
	void undo();
	public void commit();
	public void rollback();
	void markAsNew(Entity entity, UnitOfWorkRepository repo);
	void markAsDeleted(Entity entity, UnitOfWorkRepository repo);
	void markAsChanged(Entity entity, UnitOfWorkRepository repo);
	void markAsDirty(Entity entity, UnitOfWorkRepository repository);
	
}
